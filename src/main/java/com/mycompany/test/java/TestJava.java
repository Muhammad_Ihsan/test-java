/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Muhammad Ihsan
 */
public class TestJava {

    public static void main(String[] args) {
        
        System.out.println("Soal No 1");
        soal1();
        /*example :
        Input : 1,2,3,4*/
        
        System.out.println("\nSoal No 2");
        soal2();
        /*example :
        Nums : 1,2,3,4
        X : 4*/
        
        System.out.println("\nSoal No 3");
        soal3();
    }

    private static void soal1() {

        Scanner myObj = new Scanner(System.in);
        
        System.out.println("Input : ");
        String[] datas = myObj.nextLine().split(",");
        
        int result = 0;
        boolean valid;
        for (String strAngka : datas) {
            valid = true;
            int angka = Integer.parseInt(strAngka);
            for (String strAngka2 : datas) {
                int angka2 = Integer.parseInt(strAngka2);
                if ((angka-angka2) < 0) {
                    valid = false;
                    break;
                }
            }
            if (valid) {
                result = angka;
            }
        }
        System.out.println("Output : "+result);
    }

    private static void soal2() {

        Scanner myObj = new Scanner(System.in);
        
        System.out.println("Nums : ");
        String[] datas = myObj.nextLine().split(",");  
        System.out.println("X : ");
        String data2 = myObj.nextLine();
        
        List<String> result = new ArrayList<>();        
        for (String data : datas) {
            if (!data.equals(data2)) {
                result.add(data);
            }
        }        
        System.out.println("Output : "+result);
    }

    private static void soal3() {

        Scanner myObj = new Scanner(System.in);
        
        System.out.println("Word : ");
        String[] datas = myObj.nextLine().split(" "); 
        System.out.println("X : ");
        int data2 = myObj.nextInt();
        List<String> result = new ArrayList<>();
        
        for (String data : datas) {
            if (data.length() == data2) {
                result.add(data);
            }
        }        
        System.out.println("Output : "+result);
    }
}